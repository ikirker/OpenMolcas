.. _references:

Bibliography
============

.. bibliography:: molcas.bib
   :style: molcas
