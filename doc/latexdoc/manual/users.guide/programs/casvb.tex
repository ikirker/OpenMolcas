% casvb.tex $ this file belongs to the Molcas repository $

\section{\program{casvb}}
\label{UG:sec:casvb}
\index{Program!Casvb@\program{Casvb}}\index{Casvb@\program{Casvb}}
% The input format of CASVB is not completely compatible with the XML specification
% format used by MolGUI. May keywords are given the kind "STRINGS", to allow arbitrary
% content and repetitions.
%%%<MODULE NAME="CASVB">
%%Description:
%%%<HELP>
%%+CASVB is a program for performing general valence bond calculations.
%%+It can be used in two basic modes:
%%+a) variational optimization of quite general types of
%%+nonorthogonal MCSCF or modern valence bond wavefunctions, or
%%+b) representation of CASSCF wavefunctions in modern valence form,
%%+using overlap- (relatively inexpensive) or energy-based criteria.
%%%</HELP>

This program can be used in two basic modes:
\begin{itemize}
\item[a)] variational optimization of quite general types of
nonorthogonal MCSCF or modern valence bond wavefunctions
\item[b)] representation of CASSCF wavefunctions in modern valence form,
using overlap- ({\em relatively inexpensive\/}) or energy-based criteria.
\end{itemize}

For generating representations of CASSCF wavefunctions, the program
is invoked by the command {\tt CASVB}.
For variational optimization of wavefunctions it is normally invoked
inside \program{RASSCF} by the sub-command {\tt VB} (see \ref{vbinrasscf}).

Bibliography: see~\cite{casvb1,casvb2,casvb3,casvb4}.

\subsection{Dependencies}
\label{UG:sec:casvb_dependencies}
\index{Dependencies!CASVB}\index{CASVB!Dependencies}
The \program{CASVB} program needs the \file{JOBIPH} file from a \program{RASSCF} calculation,
and in addition also the \file{ONEINT} and \file{ORDINT} files from \program{SEWARD}.

\subsection{Files}
\label{UG:sec:casvb_files}
\index{Files!CASVB}\index{CASVB!Files}
\subsubsection{Input files}
\program{CASVB} will use the following input
files: \file{ONEINT}, \file{ORDINT}, \file{RUNFILE}, \file{JOBIPH},
(for more information see~\ref{UG:sec:files_list}), and
\file{VBWFN} with
valence bond wavefunction information (orbital and structure coefficients).

\subsubsection{Output files}
\begin{filelist}
%---
\item[JOBIPH]
On exit, the \program{RASSCF} interface file is overwritten with the
CASVB wavefunction.
%---
\item[VBWFN]
Valence bond wavefunction information (orbital and structure coefficients).
%---
\end{filelist}

\subsection{Input}
\index{Input!CASVB}\index{CASVB!Input}
\label{UG:sec:casvb_input}

This section describes the input to the \program{CASVB} program.
The input for each module is preceded by its name like:
\begin{inputlisting}
 &CASVB
\end{inputlisting}

\subsubsection{Keywords}
\index{Keywords!CASVB}\index{CASVB!Keywords}

Optional keywords
\begin{keywordlist}
\item[END of Input]
%%%<KEYWORD MODULE="CASVB" NAME="END" APPEAR="End of input" KIND="SINGLE" LEVEL="BASIC">
%%Keyword: END of Input <basic>
%%%<HELP>
%%+This marks the end of the input to the program and is the only compulsory
%%+keyword.
%%%</HELP></KEYWORD>
This marks the end of the input to the program.
\end{keywordlist}


Optional keywords to define the CASSCF wavefunction. Not generally required
because values stored in the job interface
file or used by the \program{RASSCF} program will normally be appropriate.
\begin{keywordlist}
%---
\item[FROZen]
%%%<KEYWORD MODULE="CASVB" NAME="FROZEN" APPEAR="Frozen orbitals" KIND="INTS_LOOKUP" SIZE="NSYM" LEVEL="ADVANCED" MIN_VALUE="0">
%%Keyword: FROZen <advanced>
%%%<HELP>
%%+Specifies frozen orbitals, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies frozen orbitals, as in the \program{RASSCF} program.
%---
\item[INACtive]
%%%<KEYWORD MODULE="CASVB" NAME="INACTIVE" APPEAR="Inactive orbitals" KIND="INTS_LOOKUP" SIZE="NSYM" LEVEL="ADVANCED" MIN_VALUE="0">
%%Keyword: INACtive <advanced>
%%%<HELP>
%%+Specifies inactive orbitals, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies inactive orbitals, as in the \program{RASSCF} program.
%---
\item[NACTel]
%%%<KEYWORD MODULE="CASVB" NAME="NACTEL" APPEAR="Active electrons" KIND="INTS" SIZE="3" LEVEL="ADVANCED" MIN_VALUE="0">
%%Keyword: NACTel <advanced>
%%%<HELP>
%%+Specifies number of active electrons, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies the number of active electrons, as in the \program{RASSCF} program.
%---
\item[RAS2]
%%%<KEYWORD MODULE="CASVB" NAME="RAS2" APPEAR="RAS2" LEVEL="ADVANCED" KIND="INTS_LOOKUP" SIZE="NSYM" DEFAULT_VALUE="0" MIN_VALUE="0">
%%Keyword: RAS2 <advanced>
%%%<HELP>
%%+Specifies RAS2 orbitals, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies RAS2 orbitals, as in the \program{RASSCF} program.
%---
\item[SPIN]
%%%<KEYWORD MODULE="CASVB" NAME="SPIN" APPEAR="Spin" LEVEL="ADVANCED" KIND="INT" DEFAULT_VALUE="1" MIN_VALUE="1">
%%Keyword: SPIN <advanced>
%%%<HELP>
%%+Specifies the total spin, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies the total spin, as in the \program{RASSCF} program.
%---
\item[SYMMetry]
%%%<KEYWORD MODULE="CASVB" NAME="SYMMETRY" APPEAR="Symmetry" LEVEL="ADVANCED" KIND="INT" DEFAULT_VALUE="1" MIN_VALUE="1" MAX_VALUE="8">
%%Keyword: SYMMetry <advanced>
%%%<HELP>
%%+Specifies the CASSCF wavefunction symmetry, as in the RASSCF program. This
%%+keyword is generally not required because the value stored in the job
%%+interface file or used by the RASSCF program will normally be appropriate.
%%%</HELP></KEYWORD>
Specifies the CASSCF wavefunction symmetry, as in the \program{RASSCF} program.
%---
\end{keywordlist}


Optional keywords to define the VB wavefunction
\begin{keywordlist}
%---
\item[CON]
%%%<KEYWORD MODULE="CASVB" NAME="CON" APPEAR="Configurations" LEVEL="BASIC" KIND="STRING">
%%Keyword: CON <basic>
%%%<HELP>
%%+Specifies spatial VB configurations in terms of the active orbitals.
%%+The default is a single configuration of singly-occupied orbitals.
%%%</HELP></KEYWORD>
The spatial
VB configurations are defined in terms of the active orbitals, and may be
specified using one or more {\tt CON} keywords:

\keyword{CON}\\
$n_1$ $n_2$ $n_3$ $n_4$ \ldots
\index{CON@{\tt CON}}

The configurations can be specified by occupation numbers, so that
$n_i$ is the occupation of the $i$th valence bond orbital. Alternatively a list of
$Nact$ orbital numbers (in any order) may be provided -- the
program determines which definition applies. The two specifications {\tt 1 0 1 2}
and {\tt 1 3 4 4} are thus equivalent.

Input configurations are reordered by \program{CASVB}, so that configurations have
non-decreasing double occupancies. Configurations that are inconsistent with the
value for the total spin are ignored.

If no configurations are specified the single `covalent' configuration
$\phi_1\phi_2\cdots\phi_{Nact}$ is assumed.
%---
\item[COUPle]
%%%<KEYWORD MODULE="CASVB" NAME="COUPLE" APPEAR="Couple scheme" LEVEL="ADVANCED" KIND="CHOICE" LIST="KOTANI,RUMER,PROJECT,LTRUMER" DEFAULT_VALUE="KOTANI">
%%Keyword: COUPle <advanced>
%%%<HELP>
%%+Specifies the scheme for constructing the spin eigenfunctions to be used.
%%+Possible values: KOTANI (default), RUMER, PROJECT, LTRUMER
%%%</HELP></KEYWORD>
\keyword{COUPLE}\\
{\em key}
\index{COUPLE@{\tt COUPLE}}

{\em key\/} may be chosen from {\tt KOTANI} (default), {\tt RUMER}, {\tt PROJECT} or {\tt LTRUMER},
specifying the scheme for constructing the
spin eigenfunctions used in the definition of valence bond structures. {\tt PROJECT}
refers to spin functions generated using a spin projection operator, {\tt LTRUMER} to
Rumer functions with the so-called ``leading term" phase convention.
%---
\item[WAVE]
%%%<KEYWORD MODULE="CASVB" NAME="WAVE" APPEAR="Wavefunction" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: WAVE <advanced>
%%%<HELP>
%%+Specifies number of electrons and spins to be used with a configuration list.
%%+Defaults are the values used by RASSCF.
%%%</HELP></KEYWORD>
\keyword{WAVE}\\
$N$ $S_1$ $S_2$ \ldots
\index{WAVE@{\tt WAVE}}

This keyword can be used to specify explicitly the number of electrons and spin(s) to
be used with a configuration list. If $N$ is less than the present number of active electrons,
the input wavefunction fragment is assumed to form part of a direct product. Otherwise, the spins
specified may be greater than or equal to the \keyword{SPIN} value specified as input to the \program{RASSCF}
program. Defaults, for both $N$ and $S$, are the values used by \program{RASSCF}.
%---
\end{keywordlist}


Optional keywords for the recovery and/or storage of orbitals and vectors
\begin{keywordlist}
%---
\item[STARt]
%%%<KEYWORD MODULE="CASVB" NAME="START" APPEAR="Input files" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: STARt <advanced>
%%%<HELP>
%%+Specifies various input files. Default is to take the required information
%%+from JOBOLD.
%%%</HELP></KEYWORD>
\keyword{START}\\
{\em key}-1={\em filename}-1\\
{\em key}-2={\em filename}-2\\
\ldots\\
\index{START@{\tt START}}

Specifies input files for VB wavefunction ({\em key}-{\em i} = VB),
CASSCF CI vector ({\em key}-{\em i} = CI) and/or CASSCF molecular orbitals
({\em key}-{\em i} = MO).
By default, the required information is taken from the file \file{JOBOLD}.
%---
\item[SAVE]
%%%<KEYWORD MODULE="CASVB" NAME="SAVE" APPEAR="Output files" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: SAVE <advanced>
%%%<HELP>
%%+Specifies output files. By default, the VB CI vector is written to the
%%+file JOBIPH.
%%%</HELP></KEYWORD>
\keyword{SAVE}\\
{\em key}-1={\em filename}-1\\
{\em key}-2={\em filename}-2\\
\ldots\\
\index{SAVE@{\tt SAVE}}

Specifies output files for VB wavefunction ({\em key}-{\em i} = VB)
and/or the VB CI vector ({\em key}-{\em i} = VBCI). By default, the VB CI
vector is written to the file JOBIPH.
%---
\end{keywordlist}


Optional keywords to override the starting guess
\begin{keywordlist}
%---
\item[GUESs]
%%%<GROUP MODULE="CASVB" NAME="GUESS" APPEAR="Guess" KIND="BLOCK" LEVEL="BASIC">
%%Keyword: GUESs <basic>
%%%<HELP>
%%+Initiates guess input. Sub-keywords are ORB and STRUC, as described in the
%%+manual. The ENDGUESs keyword terminates the guess input.
%%%</HELP>
%%%<KEYWORD MODULE="CASVB" NAME="ORB" APPEAR="Orbital" LEVEL="BASIC" KIND="STRINGS">
%%%</KEYWORD>
%%%<KEYWORD MODULE="CASVB" NAME="STRUC" APPEAR="Structure" LEVEL="BASIC" KIND="STRINGS">
%%%</KEYWORD>
%%%</GROUP>

\keyword{GUESS}\\
{\em key-1 \ldots}\\
{\em key-2 \ldots}\\
{\tt ENDGUESs}\\
\index{GUESS@{\tt GUESS}}

The \keyword{GUESS} keyword initiates the input of a guess for the valence bond orbitals and/or
structure coefficients. {\em key-i\/} can be either \keyword{ORB} or \keyword{STRUC}.
These keywords
modify the guess provided by the program. It is
thus possible to modify individual orbitals in a previous solution
so as to construct the starting
guess. The \keyword{ENDGUESs} keyword terminates the guess input.

\keyword{ORB}\\
{\em i  c$_1$ c$_2$ \ldots c$_{mact}$}

Specifies a starting guess for valence bond orbital number $i$. The guess is specified
in terms of the $mact$ active MOs defining the CASSCF wavefunction.

\keyword{STRUC}\\
{\em c$_1$ c$_2$ \ldots c$_{NVB}$}

Specifies a starting guess for the $NVB$ structure coefficients. If this keyword
is not provided, the perfect-pairing mode of
spin coupling is assumed for the spatial configuration having the least
number of doubly occupied orbitals.
Note that the definition of structures depends on the value of {\tt COUPLE}. Doubly occupied
orbitals occur first in all configurations, and the spin eigenfunctions are based on the singly
occupied orbitals being in ascending order.
%---
\item[ORBPerm]
%%%<KEYWORD MODULE="CASVB" NAME="ORBPERM" APPEAR="Orbital permutation" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: ORBPerm <advanced>
%%%<HELP>
%%+Permutes the VB orbitals, and modifies phases, as described in the manual.
%%%</HELP></KEYWORD>
\keyword{ORBPERM}\\
$i_1$ \ldots $i_{mact}$
\index{ORBPERM@{\tt ORBPERM}}

Permutes the orbitals in the valence bond wavefunction and changes their phases according to
$\phi_j'={\rm sign}(i_j)\phi_{{\rm abs}(i_j)}$. The guess may be further modified using the
\keyword{GUESS} keyword. Additionally, the structure coefficients will be transformed
according to the given permutation (note that the configuration list must be closed under
the orbital permutation for this to be possible).
%---
\end{keywordlist}


Optional keywords for optimization control
\begin{keywordlist}
%---
\item[CRIT]
%%%<KEYWORD MODULE="CASVB" NAME="CRIT" APPEAR="Optimization criterion" LEVEL="BASIC" KIND="CHOICE" LIST="OVERLAP,ENERGY" DEFAULT_VALUE="OVERLAP">
%%Keyword: CRIT <basic>
%%%<HELP>
%%+Defines the optimization criterion.
%%+Possible values: OVERLAP (default) or ENERGY.
%%%</HELP></KEYWORD>
\keyword{CRIT}\\
{\em method\/}
\index{CRIT@{\tt CRIT}}

Specifies the criterion for the optimization. {\em method\/} can be \keyword{OVERLAP} or \keyword{ENERGY\/}
(\keyword{OVERLAP} is default).
The former maximizes the normalized overlap with the CASSCF wavefunction:
\[
{\rm max}\left(\frac{\langle\Psi_{CAS}|\Psi_{VB}\rangle}{(\langle\Psi_{VB}|\Psi_{VB}\rangle)^{1/2}}\right)
\]
and the latter simply minimizes the energy:
\[
{\rm min}\left(\frac{\langle\Psi_{VB}|\hat{H}|\Psi_{VB}\rangle}{\langle\Psi_{VB}|\Psi_{VB}\rangle}\right).
\]
%---
\item[MAXIter]
%%%<KEYWORD MODULE="CASVB" NAME="MAXITER" APPEAR="Maximum iterations" LEVEL="ADVANCED" KIND="INT" DEFAULT_VALUE="50" MIN_VALUE="1">
%%Keyword: MAXIter <advanced>
%%%<HELP>
%%+Specifies the maximum number of iterations to be used. Default value is 50.
%%%</HELP></KEYWORD>
\keyword{MAXITER}\\
{$N_{iter}$}
\index{MAXITER@{\tt MAXITER}}

Specifies the maximum number of iterations in the second-order optimizations. Default is $N_{iter}$=50.
%---
\item[(NO)CASProj]
%%%<KEYWORD MODULE="CASVB" NAME="CASPROJ" APPEAR="CAS proj" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="NOCASPROJ">
%%Keyword: CASProj <advanced>
%%%<HELP>
%%+Defines structure coefficients from transformed CASSCF wavefunction.
%%%</HELP></KEYWORD>
%
%%%<KEYWORD MODULE="CASVB" NAME="NOCASPROJ" APPEAR="No CAS proj" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="CASPROJ">
%%Keyword: NOCASProj <advanced>
%%%<HELP>
%%+Disables CASProj
%%%</HELP></KEYWORD>

\keyword{(NO)CASPROJ}
\index{CASPROJ@{\tt CASPROJ}}
\index{NOCASPROJ@{\tt NOCASPROJ}}

With this keyword the structure coefficients are picked from the transformed CASSCF CI vector, leaving
only the orbital variational parameters. For further details see the bibliography.
This option may be useful to aid convergence.
%---
\item[SADDle]
%%%<KEYWORD MODULE="CASVB" NAME="SADDLE" APPEAR="Saddle point" LEVEL="ADVANCED" KIND="INT" MIN_VALUE="1">
%%Keyword: SADDLe <advanced>
%%%<HELP>
%%+Defines optimization onto an n-th order saddle point.
%%%</HELP></KEYWORD>
\keyword{SADDLE}\\
{\em n}
\index{SADDLE@{\tt SADDLE}}

Defines optimization onto an {\em n}$^{\rm th}$-order saddle point.
See also T.~Thorsteinsson and D.~L.~Cooper, Int.\ J.\ Quant.\ Chem.\
{\bf 70}, 637--50 (1998).
%---
\item[(NO)INIT]
%%%<KEYWORD MODULE="CASVB" NAME="INIT" APPEAR="Initial optimizations" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="NOINIT">
%%Keyword: INIT <advanced>
%%%<HELP>
%%+Requests a sequence of preliminary optimizations which aim to minimize
%%+the computational cost while maximizing the likelihood of stable
%%+convergence. This is the default behaviour when no wavefunction guess is
%%+available and no OPTIM keyword has been specified.
%%%</HELP></KEYWORD>
%
%%%<KEYWORD MODULE="CASVB" NAME="NOINIT" APPEAR="No initial optimizations" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="INIT">
%%Keyword: NOINIT <advanced>
%%%<HELP>
%%+Disables INIT
%%%</HELP></KEYWORD>
\keyword{(NO)INIT}
\index{INIT@{\tt INIT}}
\index{NOINIT@{\tt NOINIT}}

Requests a sequence of preliminary optimizations which aim to minimize the
computational cost while maximizing the likelihood of stable
convergence. This feature is the default if no wavefunction guess is available
and no \keyword{OPTIM} keyword specified in the input.
%---
\item[METHod]
%%%<KEYWORD MODULE="CASVB" NAME="METHOD" APPEAR="Method" LEVEL="ADVANCED" KIND="CHOICE" LIST="FLETCHER,TRIM,TRUSTOPT,DAVIDSON,STEEP,VB2CAS,AUGHESS,AUG2,CHECK,DFLETCH,NONE,SUPER">
%%Keyword: METHod <advanced>
%%%<HELP>
%%+Selects optimization algorithm.
%%+Possible values: FLETCHER, TRIM, TRUSTOPT, DAVIDSON, STEEP, VB2CAS,
%%+AUGHESS, AUG2, CHECK, DFLETCH, NONE or SUPER.
%%+The default algorithm chosen by CASVB will usually be adequate.
%%%</HELP></KEYWORD>
\keyword{METHOD}\\
{\em key}
\index{METHOD@{\tt METHOD}}

Selects the optimization algorithm to be used. {\em key} can be one
of: \keyword{FLETCHER}, \keyword{TRIM}, \keyword{TRUSTOPT}, \keyword{DAVIDSON},
\keyword{STEEP}, \keyword{VB2CAS}, \keyword{AUGHESS}, \keyword{AUG2},
\keyword{CHECK}, \keyword{DFLETCH}, \keyword{NONE}, or \keyword{SUPER}. Recommended are
the direct procedures \keyword{DFLETCH} or \keyword{AUGHESS}. For general
saddle-point optimization \keyword{TRIM} is used. Linear (CI only) optimization
problems use \keyword{DAVIDSON}. \keyword{NONE} suspends optimization, while
\keyword{CHECK} carries out a finite-difference check of the gradient and Hessian.

The default algorithm chosen by \program{CASVB} will be usually be adequate.
%---
\item[TUNE]
%%%<KEYWORD MODULE="CASVB" NAME="TUNE" APPEAR="Tune" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: TUNE <advanced>
%%%<HELP>
%%+Enables the input of individual parameters to be used in the optimization procedure. Expert use only. See manual.
%%%</HELP></KEYWORD>
\keyword{TUNE}\\
\ldots
\index{TUNE@{\tt TUNE}}

Enables the input of individual parameters to be used in the optimization procedure
({\em e.g.}\/ for controlling step-size selection and convergence testing).
Details of the values used are output if {\em print}(3)$\geq$3 is specified.
For expert use only.
%---
\item[OPTIm]
%%%<KEYWORD MODULE="CASVB" NAME="OPTIM" APPEAR="Optimizations" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: OPTIm <advanced>
%%%<HELP>
%%+Defines one or more optimization steps. Subcommands can be any
%%+optimization declarations, as well as any symmetry or constraints
%%+specifications. Usually omitted if only one optimization step is required.
%%+Terminated by the keyword ENDOPTIm.
%%%</HELP></KEYWORD>
More than one optimization may be performed in the same \program{CASVB} run,
by the use of \keyword{OPTIM} keywords:

\keyword{OPTIM}\\
{[}\ldots \\
\keyword{ENDOPTIM}{]}
\index{OPTIM@{\tt OPTIM}}

The subcommands may be any optimization declarations defined in this
section, as well as any symmetry or constraints specifications.
Commands given as arguments to \keyword{OPTIM}
will apply only to this optimization step, whereas commands specified
outside will act as default definitions for all subsequent {\tt OPTIM}
specifications.

The \keyword{OPTIM} keyword
need not be specified if only one optimization step is required,

When only a machine-generated guess is available, \program{CASVB} will
attempt to
define a sequence of optimization steps that aims to maximize the
likelihood of successful convergence (while minimizing
CPU usage). To override this behaviour, simply specify one or more
\keyword{OPTIM} keywords. The \keyword{ENDOPTIm} keyword marks the end of the
specifications of an optimization step.
%---
\item[ALTErn]
%%%<KEYWORD MODULE="CASVB" NAME="ALTERN" APPEAR="Alternate" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: ALTErn <advanced>
%%%<HELP>
%%+Defines alternating optimizations over two or more optimization steps (see
%%+manual). Terminated by the ENDALTErn keyword.
%%%</HELP></KEYWORD>
A loop over two or more optimization steps may be specified using:

\keyword{ALTERN}\\
{\em Niter}\\
\ldots\\
\keyword{ENDALTERN}
\index{ALTERN@{\tt ALTERN}}

The program will repeat the specified optimization steps
until either all optimizations have converged, or the maximum iteration count,
{\em Niter}, has been reached.
The \keyword{ENDALTErn} keyword marks the end of the specification of an
ALTERN loop.
%---
\end{keywordlist}


Optional keywords for definitions of molecular symmetry and any
constraints on the VB wavefunction
\begin{keywordlist}
%---
\item[SYMElm]
%%%<KEYWORD MODULE="CASVB" NAME="SYMELM" APPEAR="Symmetry elements" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: SYMElm <advanced>
%%%<HELP>
%%+Initiates the definition of a symmetry operation (see manual).
%%+Sub-keywords are IRREPS, COEFFS, or TRANS. Terminated with ENDSYMElm.
%%%</HELP></KEYWORD>
Various issues associated with symmetry-adapting valence bond wavefunctions
are considered, for example, in: T.~Thorsteinsson, D.~L.~Co\-oper,
J.~Gerratt and M.~Raimondi, Theor.\ Chim.\ Acta {\bf 95}, 131 (1997).

\keyword{SYMELM}\\
{\em label} {\em sign}
\index{SYMELM@{\tt SYMELM}}

Initiates the definition of a symmetry operation referred to by {\em label\/} (any three characters).
{\em sign\/} can be + or $-$; it specifies whether the total wavefunction is symmetric or
antisymmetric under this operation, respectively. A value for {\em sign\/} is not always necessary
but, if provided, constraints will be put on the structure coefficients to ensure that the
wavefunction has the correct overall symmetry (note that the configuration list must be closed
under the orbital permutation induced by {\em label\/} for this to be possible).
The default for {\em label\/} is the identity.

The operator is defined in terms of its action on the active MOs as specified by
one or more of the keywords \keyword{IRREPS}, \keyword{COEFFS}, or \keyword{TRANS}. Any
other keyword, including optional use of the \keyword{ENDSYMElm} keyword, will
terminate the definition of this symmetry operator.

\keyword{IRREPS}\\
{\em i$_1$ i$_2$ \ldots}

The list {\em i$_1$ i$_2$ \ldots} specifies which irreducible representations (as defined in
the CASSCF wavefunction) are antisymmetric with respect to the {\em label\/} operation.
If an irreducible representation is not otherwise specified it is assumed to be symmetric
under the symmetry operation.

\keyword{COEFFS}\\
{\em i$_1$ i$_2$ \ldots}

The list {\em i$_1$ i$_2$ \ldots} specifies which individual CASSCF MOs are antisymmetric with
respect to the {\em label\/} operation. If an MO is not otherwise specified, it is assumed to be
symmetric under the symmetry operation. This specification may be useful if, for example, the
molecule possesses symmetry higher than that exploited in the CASSCF calculation.

\keyword{TRANS}\\
{\em n$_{dim}$ i$_1$ \ldots i$_{n_{dim}}$ c$_{11}$ c$_{12}$
\ldots c$_{n_{dim}n_{dim}}$}

Specifies a general n$_{dim}\times n_{dim}$ transformation involving the MOs {\em i$_1$,
\ldots i$_{n_{dim}}$},
specified by the $c$ coefficients. This may be useful for systems with a two- or
three-dimensional irreducible representation, or if localized orbitals define the CASSCF
wavefunction. Note that the specified transformation must always be orthogonal.
%---
\item[ORBRel]
%%%<KEYWORD MODULE="CASVB" NAME="ORBREL" APPEAR="Orbital relations" LEVEL="ADVANCED" KIND="STRINGS">
%%Keyword: ORBRel <advanced>
%%%<HELP>
%%+Specifies the relationship between two VB orbitals under symmetry
%%+operation(s) defined by SYMElm. See manual.
%%%</HELP></KEYWORD>
In general, for a VB wavefunction to be symmetry-pure, the orbitals must form a representation
(not necessarily irreducible) of the symmetry group. Relations between orbitals under
the symmetry operations defined by {\tt SYMELM} may be specified according to:

\keyword{ORBREL}\\
{\em i$_1$ i$_2$ label1 label2 \ldots}
\index{ORBREL@{\tt ORBREL}}

Orbital $i_1$ is related to orbital $i_2$ by the sequence of operations defined by the {\em label\/}
specifications (defined previously using {\tt SYMELM}). The operators operate right to left. Note
that $i_1$ and $i_2$ may coincide. Only the minimum number of
relations required to define all the orbitals should be provided; an error exit
will occur if redundant {\tt ORBREL} specifications are found.
%---
\item[(NO)SYMProj]
%%%<KEYWORD MODULE="CASVB" NAME="SYMPROJ" APPEAR="Symmetry projection" LEVEL="ADVANCED" KIND="INTS_LOOKUP" SIZE="NSYM" EXCLUSIVE="NOSYMPROJ">
%%Keyword: SYMProj <advanced>
%%%<HELP>
%%+Projects the VB wavefunction onto given irrep(s). See manual.
%%%</HELP></KEYWORD>
%
%%%<KEYWORD MODULE="CASVB" NAME="NOSYMPROJ" APPEAR="No symmetry projection" LEVEL="ADVANCED" KIND="INTS_LOOKUP" SIZE="NSYM" EXCLUSIVE="SYMPROJ">
%%Keyword: NoSYMProj <advanced>
%%%<HELP>
%%+Disables SYMProj
%%%</HELP></KEYWORD>
As an alternative to incorporating constraints, one may also ensure correct
symmetry of the wavefunction by use of a projection operator:

\keyword{(NO)SYMPROJ}\\
{[}{\em irrep}$_1$ {\em irrep}$_2$ \ldots{]}
\index{SYMPROJ@{\tt SYMPROJ}}
\index{NOSYMPROJ@{\tt NOSYMPROJ}}

The effect of this keyword is to set to zero the coefficients in unwanted
irreducible representations.
For this purpose, the symmetry group defined for the CASSCF wavefunction
is used (always a subgroup of D$_{2h}$).
The list of irreps in the command specifies which components
of the wavefunction should be kept.
If no irreducible representations are given, the current
wavefunction symmetry is assumed. In a state-averaged calculation,
all irreps are retained for which a non-zero weight has been specified in the
wavefunction definition.
The \keyword{SYMPROJ} keyword may also be used in combination with constraints.
%---
\item[FIXOrb]
%%%<KEYWORD MODULE="CASVB" NAME="FIXORB" APPEAR="Freeze orbitals" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: FIXOrb <advanced>
%%%<HELP>
%%+Freezes a subset of VB orbitals (i1, i2, ...).
%%%</HELP></KEYWORD>
\keyword{FIXORB}\\
{\em i$_1$ i$_2$ \ldots}
\index{FIXORB@{\tt FIXORB}}

This command freezes the orbitals specified in the list
{\em i$_1$ i$_2$ \ldots} to that of the starting guess. Alternatively the
special keywords \keyword{ALL} or \keyword{NONE} may be used. These orbitals
are eliminated from the optimization procedure, but will still be
normalized and symmetry-adapted according to any \keyword{ORBREL}
keywords given.
%---
\item[FIXStruc]
%%%<KEYWORD MODULE="CASVB" NAME="FIXSTRUC" APPEAR="Freeze coefficients" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: FIXStruc <advanced>
%%%<HELP>
%%+Freezes a subset of structure coefficients (i1, i2, ...).
%%%</HELP></KEYWORD>
\keyword{FIXSTRUC}\\
{\em i$_1$ i$_2$ \ldots}
\index{FIXSTRUC@{\tt FIXSTRUC}}

Freezes the coefficients for structures {\em i$_1$, i$_2$,\ldots}. Alternatively
the special keywords \keyword{ALL} or \keyword{NONE} may be used. The
structures are eliminated from the optimization procedure, but may
still be affected by normalization or any symmetry keywords present.
%---
\item[DELStruc]
%%%<KEYWORD MODULE="CASVB" NAME="DELSTRUC" APPEAR="Delete structures" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: DELStruc <advanced>
%%%<HELP>
%%+Deletes a subset of structures from the wavefunction (i1, i2, ...). Other
%%+possible values: ALL or NONE.
%%%</HELP></KEYWORD>
\keyword{DELSTRUC}\\
{\em i$_1$ i$_2$,\ldots}
\index{DELSTRUC@{\tt DELSTRUC}}

Deletes the specified structures from the wavefunction. The
special keywords \keyword{ALL} or \keyword{NONE} may be used. This specification should be compatible
with the other structure constraints present, as defined by \keyword{SYMELM} and \keyword{ORBREL}.
%---
\item[ORTHcon]
%%%<GROUP MODULE="CASVB" NAME="ORTHCON" APPEAR="Orthogonality constraints" KIND="BLOCK" LEVEL="ADVANCED">
%%Keyword: ORTHcon <advanced>
%%%<HELP>
%%+Initiates input of orthogonality constraints information.
%%+Sub-keywords are ORTH, PAIRS, GROUP, STRONG and FULL, as described in the
%%+manual. The ENDORTHcon keyword terminates the ORTHcon input.
%%%</HELP>
%%%<KEYWORD MODULE="CASVB" NAME="ORTH" APPEAR="Orbitals" LEVEL="ADVANCED" KIND="STRINGS">
%%%</KEYWORD>
%%%<KEYWORD MODULE="CASVB" NAME="PAIRS" APPEAR="Pairs" LEVEL="ADVANCED" KIND="STRINGS">
%%%</KEYWORD>
%%%<KEYWORD MODULE="CASVB" NAME="GROUP" APPEAR="Group" LEVEL="ADVANCED" KIND="STRINGS">
%%%</KEYWORD>
%%%<KEYWORD MODULE="CASVB" NAME="STRONG" APPEAR="Strong orthogonality" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="FULL">
%%%</KEYWORD>
%%%<KEYWORD MODULE="CASVB" NAME="FULL" APPEAR="Full orthogonality" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="STRONG">
%%%</KEYWORD>
%%%</GROUP>
{\tt ORTHCON}\\
{\em key-1 \ldots}\\
{\em key-2 \ldots}\\
\ldots
\index{ORTHCON@{\tt ORTHCON}}

The {\tt ORTHCON} keyword initiates the input of orthogonality
constraints between pairs/groups of valence bond orbitals.
The sub-keywords {\em key-i\/} can be any of {\tt ORTH}, {\tt PAIRS},
{\tt GROUP}, {\tt STRONG} or {\tt FULL}. Orthogonality constraints
should be used with discretion. Note that orthogonality constraints
for an orbital generated from another by symmetry operations (using the
{\tt ORBREL} keyword) cannot in general be satisfied. The {\tt ENDORTHcon}
keyword can be used to terminate the input of orthogonality constraints.

{\tt ORTH}
{\em i$_1$ i$_2$ \ldots}

Specifies a list of orbitals to be orthogonalized. All overlaps
between pairs of orbitals in the list are set to zero.

{\tt PAIRS} {\em i$_1$ i$_2$ \ldots}

Specifies a simple list of orthogonalization pairs. Orbital $i_1$ is
made orthogonal to $i_2$, $i_3$ to $i_4$, etc.

{\tt GROUP} {\em label} {\em i$_1$ i$_2$ \ldots}

Defines an orbital group to be used with the {\tt ORTH} or
{\tt PAIRS} keyword. The group is referred to by {\em label\/} which
can be any three characters beginning with a letter a--z. Labels
defining different groups can be used together or in combination
with orbital numbers in {\tt ORTH} or {\tt PAIRS}.
{\em i$_1$ i$_2$ \ldots} specifies
the list of orbitals in the group. Thus the combination
{\tt GROUP} AAA 1 2 {\tt GROUP} BBB 3 4 {\tt ORTH} AAA BBB  will orthogonalize
the pairs of orbitals 1-3, 1-4, 2-3 and 2-4.

{\tt STRONG}

This keyword is short-hand for strong orthogonality. The only allowed
non-zero overlaps are between pairs of orbitals ($2n$$-$$1$, $2n$).

{\tt FULL}

This keyword is short-hand for full orthogonality and is mainly
useful for testing purposes.
%---
\end{keywordlist}


Optional keywords for wavefunction analysis
\begin{keywordlist}
%---
\item[CIWEights]
%%%<KEYWORD MODULE="CASVB" NAME="CIWEIGHTS" APPEAR="Print CI weights" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: CIWEights <advanced>
%%%<HELP>
%%+Prints weights of VB structures in the CASSCF wavefunction. Options are
%%+the same as for VBWEights.
%%%</HELP></KEYWORD>
For further details regarding the calculation of weights in \program{CASVB}, see
T.~Thorsteinsson and D.~L.~Cooper, J.\ Math.\ Chem.\ {\bf 23}, 105-26 (1998).

{\tt CIWEIGHTS}\\
{\em key1} {\em key2} \ldots [$N_{\rm conf}$]
\index{CIWEIGHTS@{\tt CIWEIGHTS}}

Prints weights of the CASSCF wavefunction transformed
to the basis of nonorthogonal VB structures. For the {\em key\/} options
see {\tt VBWEIGHTS} below. Note that the evaluation of inverse overlap
weights involves an extensive computational overhead for large active
spaces. Weights are given for the
total CASSCF wavefunction, as well as the orthogonal complement to
$\Psi_{VB}$. The default for the number of configurations requested,
$N_{\rm conf}$, is 10. If $N_{\rm conf}$=$-1$ all configurations are
included.
%---
\item[REPOrt]
%%%<KEYWORD MODULE="CASVB" NAME="REPORT" APPEAR="Report" LEVEL="ADVANCED" KIND="STRINGs">
%%Keyword: REPOrt <advanced>
%%%<HELP>
%%+Outputs orbital/structure coefficients and derived information. Terminated
%%+by ENDREPOrt.
%%%</HELP></KEYWORD>
{\tt REPORT}\\
{[}\ldots \\
{\tt ENDREPORT}{]}
\index{REPORT@{\tt REPORT}}

Outputs orbital/structure coefficients and derived information.
The {\tt ENDREPOrt} keyword can be used to mark the end of the specification
of a report step.
%---
\item[(NO)SCORr]
%%%<KEYWORD MODULE="CASVB" NAME="SCORR" APPEAR="Spin values" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="NOSCORR">
%%Keyword: SCORr <advanced>
%%%<HELP>
%%+Performs spin-correlation analysis. Only implemented for spin-coupled
%%+wavefunctions
%%%</HELP></KEYWORD>
%
%%%<KEYWORD MODULE="CASVB" NAME="NOSCORR" APPEAR="No spin values" LEVEL="ADVANCED" KIND="SINGLE" EXCLUSIVE="SCORR">
%%Keyword: NOSCORr <advanced>
%%%<HELP>
%%+Disables SCORr
%%%</HELP></KEYWORD>
({\tt NO}){\tt SCORR}
\index{SCORR@{\tt SCORR}}

With this option, expectation values of the spin operators
$(\hat{s}_\mu+\hat{s}_\nu)^2$ are evaluated for all pairs of $\mu$ and
$\nu$. Default is {\tt NOSCORR}. The procedure is described by: G.~Raos,
J.~Gerratt, D.~L.~Cooper and M.~Raimondi,
Chem.\ Phys.\ {\bf 186}, 233--250 (1994); ibid, 251--273 (1994);
D.~L.~Cooper, R.~Ponec, T.~Thorsteinsson and G.~Raos,
Int.\ J.\ Quant.\ Chem.\ {\bf 57}, 501--518 (1996).

This analysis is currently only implemented for spin-coupled wavefunctions.
%---
\item[VBWEights]
%%%<KEYWORD MODULE="CASVB" NAME="VBWEIGHTS" APPEAR="Print VB weights" LEVEL="ADVANCED" KIND="STRING">
%%Keyword: VBWEights <advanced>
%%%<HELP>
%%+Prints weights of VB structures.
%%+Possible values CHIRGWIN, LOWDIN, INVERSE, ALL and NONE.
%%%</HELP></KEYWORD>
For further details regarding the calculation of weights in \program{CASVB}, see
T.~Thorsteinsson and D.~L.~Cooper, J.\ Math.\ Chem.\ {\bf 23}, 105-26 (1998).

{\tt VBWEIGHTS}\\
{\em key1} {\em key2} \ldots
\index{VBWEIGHTS@{\tt VBWEIGHTS}}

Calculates and outputs weights of the structures in the valence bond
wavefunction $\Psi_{VB}$. {\em key\/} specifies the definition of
nonorthogonal weights to be used, and can be one of:
\begin{description}
\item[{\tt CHIRGWIN}] Evaluates Chirgwin-Coulson weights (see:
B.~H.~Chirgwin and C.~A.~Coulson, Proc.\ Roy.\ Soc.\ Lond.\ {\bf A201},
196 (1950)).
\item[{\tt LOWDIN}] Performs a symmetric orthogonalization of the
structures and outputs the subsequent weights.
\item[{\tt INVERSE}] Outputs ``inverse overlap populations" as in
G.~A.~Gallup and J.~M.~Norbeck, Chem.\ Phys.\ Lett.\ {\bf 21}, 495--500 (1973).
\item[{\tt ALL}] All of the above.
\item[{\tt NONE}] Suspends calculation of structure weights.
\end{description}
The commands {\tt LOWDIN} and {\tt INVERSE} require the overlap matrix
between valence bond structures, so that some additional computational
overhead is involved.
%---
\end{keywordlist}


Optional keywords for further general options
\begin{keywordlist}
%---
\item[PREC]
%%%<KEYWORD MODULE="CASVB" NAME="PREC" APPEAR="Print precision" LEVEL="ADVANCED" KIND="INTS" SIZE="2" DEFAULT_VALUES="8,110" MIN_VALUE="0">
%%Keyword: PREC <basic>
%%%<HELP>
%%+Adjusts the precision for printed quantities. See manual.
%%%</HELP></KEYWORD>
{\tt PREC}\\
{\em iprec} {\em iwidth}
\index{PREC@{\tt PREC}}

Adjusts the precision for printed quantities. In most cases, {\em iprec}\/ simply refers
to the number of significant digits after the decimal point. Default is {\em iprec}=+8.
{\em iwidth}\/ specifics the maximum width of printed output, used when determining
the format for printing arrays.
%---
\item[PRINt]
%%%<KEYWORD MODULE="CASVB" NAME="PRINT" APPEAR="Print levels" LEVEL="BASIC" KIND="INTS" SIZE="7" DEFAULT_VALUES="1,1,1,1,1,1,1" MIN_VALUE="-1" MAX_VALUE="2">
%%Keyword: PRINT <basic>
%%%<HELP>
%%+Controls the amount of output. See manual.
%%%</HELP></KEYWORD>
{\tt PRINT}\\
{\em i$_1$ i$_2$ \ldots}
\index{PRINT@{\tt PRINT}}

Each number specifies the level of output required at various stages of the execution, according to the
following convention:
\begin{description}
\item[{\mdseries{-1}}]No output except serious, or fatal, error messages.
\item[{\mdseries{ }0}]Minimal output.
\item[{\mdseries{ }1}]Standard level of output.
\item[{\mdseries{ }2}]Extra output.
\end{description}
The areas for which output can be controlled are:
\begin{description}
\item[$i_1$] Print of input parameters, wavefunction definitions, etc.
\item[$i_2$] Print of information associated with symmetry constraints.
\item[$i_3$] General convergence progress.
\item[$i_4$] Progress of the 2nd-order optimization procedure.
\item[$i_5$] Print of converged solution and analysis.
\item[$i_6$] Progress of variational optimization.
\item[$i_7$] File usage.
\end{description}
For all, the default output level is +1. If $i_5$$\geq$2 VB orbitals will
be printed in the AO basis (provided that the definition of MOs is
available).
%---
\item[SHSTruc]
%%%<KEYWORD MODULE="CASVB" NAME="SHSTRUC" APPEAR="Print matrices" LEVEL="ADVANCED" KIND="SINGLE">
%%Keyword: SHSTruc <advanced>
%%%<HELP>
%%+Prints overlap and Hamiltonian matrices between VB structures.
%%%</HELP></KEYWORD>
Prints overlap and Hamiltonian matrices between VB structures.
%---
\item[STATs]
%%%<KEYWORD MODULE="CASVB" NAME="STATS" APPEAR="Print statistics" LEVEL="BASIC" KIND="SINGLE">
%%Keyword: STATs <basic>
%%%<HELP>
%%+Prints timing and usage statistics.
%%%</HELP></KEYWORD>
{\tt STATS}
\index{STATS@{\tt STATS}}

Prints timing and usage statistics.
%---
\end{keywordlist}


\subsubsection{Input example}
%%%To_extract{/doc/samples/ug/CASVB.input}
\begin{inputlisting}
&seward
symmetry
x y
basis set
c.sto-3g....
c 0 0 -0.190085345
end of basis
basis set
h.sto-3g....
h 0 1.645045225 1.132564974
end of basis
&scf
occupied
3 0 1 0
&rasscf
inactive
1 0 0 0
ras2
3 1 2 0
nactel
6 0 0
lumorb
&casvb
\end{inputlisting}
%%%To_extract

\subsubsection{Viewing and plotting VB orbitals}

\index{casvb!Plotting}

In many cases it can be helpful to view the shape of the converged valence bond orbitals, and
Molcas therefore provides two facilities for doing this. For the Molden program, an interface file
is generated at the end of each \program{CASVB} run (see also Section~\ref{UG:sec:Molden}).
Alternatively a \program{CASVB} run may be followed by \program{RASSCF} to get orbitals
(Section~\ref{UG:sec:rasscf}) and \program{GRID\_IT} with the \keyword{VB} specification
(Section~\ref{UG:sec:gridit}), in order to generate a three-dimensional grid, for viewing, for example,
with \program{LUSCUS} program.
%%%</MODULE>
